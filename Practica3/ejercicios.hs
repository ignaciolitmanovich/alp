--Ejercicio 9
Y = λx.(λy.x(y y))(λy.x (y y))

a)
Y X

=== {def Y}

(λx. (λy. x (y y)) (λy. x (y y))) X

-->B

(λy. X (y y)) (λy. X (y y))

=== {def}
( λy. X (y y) ) [X (y y) / y]

-->B
(X ( (X (y y)) (X (y y) ) ) )


b)
Y 
= {def}
λx.(λy.x(y y))(λy.x (y y))

--Ejercicio 10





-- Árbol izquierdo
-- izq = N ( N ( N ( N ( N ( N ( N ( N ( N E 'e' E) 'x' E) 'd' E) 'x' E) 'y' E) 'x' E) 'x' E) 'a' E) 'x' E

-- Árbol derexo
-- der = N E 'e' ( N E 'x' ( N E 'd' ( N E 'x' ( N E 'y' ( N E 'x' ( N E 'x' ( N E 'a' ( N E 'x' E))))))))