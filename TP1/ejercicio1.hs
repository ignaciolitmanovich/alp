

intexp ::= nat
            | var
            | − intexp
            | intexp + intexp
            | intexp − intexp
            | intexp × intexp
            | intexp ÷ intexp
            | var = intexp
            | intexp , intexp

boolexp ::= true
            | false
            | intexp == intexp
            | intexp != intexp
            | intexp < intexp
            | intexp > intexp
            | boolexp ∧ boolexp
            | boolexp ∨ boolexp
            | ¬boolexp

comm ::= skip
        | var = intexp
        | comm ; comm
        | if boolexp then comm
        | if boolexp then comm else comm
        | while boolexp do comm

digit ::= ’0’ | ’1’ | · · · | ’9’
letter ::= ’a’ | · · · | ’Z’
nat ::= digit | digit nat
var ::= letter | letter var
intexp ::= nat
            | var
            | ’-’ intexp
            | intexp ’+’ intexp
            | intexp ’-’ intexp
            | intexp ’*’ intexp
            | intexp ’/’ intexp
            | ’(’ intexp ’)’
            | var ’=’ intexp
            | intexp ’,’ intexp

boolexp ::= ’true’ 
                | ’false’
                | intexp ’==’ intexp
                | intexp ’!=’ intexp
                | intexp ’<’ intexp
                | intexp ’>’ intexp
                | boolexp ’&&’ boolexp
                | boolexp ’||’ boolexp
                | ’!’ boolexp
                | ’(’ boolexp ’)’
                
comm ::= skip
        | var ’=’ intexp
        | comm ’;’ comm
        | ’if’ boolexp ’{’ comm ’}’
        | ’if’ boolexp ’{’ comm ’}’ ’else’ ’{’ comm ’}’
        | ’while’ boolexp ’{’ comm ’}’



data EAssgn = Const Int
                | Var Variable 
                | UMinus EAssgn
                | Plus EAssgn
                | Minus EAssgn EAssgn
                | Times EAssgn EAssgn
                | Div EAssgn EAssgn