p = string "hola"

p = reservedOp lis "if"

data () = ()

ass :: Parser ()
ass ? do x <- identifier lis
        reserverOp list "="
        n <- nat
        return ()

p = try (string "AB") <|> string "AA"

parser p "AA3"
"AA"

chanill p op x

e : = e '+' t
t : t '*' f
f := n

expr :: Parser Int
expr = cahinl1 term (do {reservedOp lis "+"; return (+)})

term :: Parser Int
term = cahinl1 factor (do {reservedOp lis "+"; return (*)})

factor :: Parser Int
factor = integer

parseTest p "dd"
donde parseTest es la funcion
p es el parser
y el último argumento el string.

